package com.sysomos.location.service.api.phoenix;

import java.sql.SQLException;
import java.util.List;

public interface GeoService {
	public List<location> getPredictedByLocation(String location, Integer rank, Integer count) throws SQLException;
	public List<location> getPredictedByLocation(String location, Integer rank, Integer count, Integer limit) throws SQLException;
	public List<location> getPredictedByLocation(String location, Integer rank,  Double prob, Integer count, Integer limit) throws SQLException;
	public List<location> getActualByLocation(String location, Integer rank, Integer count) throws SQLException;
	public List<location> getActualByLocation(String location, Integer rank, Integer count, Integer limit) throws SQLException;
	public List<location> getActualByLocation(String location, Integer rank, Double prob, Integer count, Integer limit) throws SQLException;
    public void close();
}