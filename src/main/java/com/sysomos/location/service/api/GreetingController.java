package com.sysomos.location.service.api;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sysomos.location.service.api.phoenix.GeoServiceImpl;
import com.sysomos.location.service.api.phoenix.location;

@RestController
public class GreetingController {

	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();

	@CrossOrigin
	@RequestMapping(value = "/{fileName:.+}", method = RequestMethod.GET)
	public String greeting(@PathVariable("fileName") String fileName) throws FileNotFoundException {
		File file = new File("./resources/" + fileName);
		StringBuilder fileContents = new StringBuilder((int) file.length());
		Scanner scanner = new Scanner(file);
		String lineSeparator = System.getProperty("line.separator");

		try {
			while (scanner.hasNextLine()) {
				fileContents.append(scanner.nextLine() + "\n");
			}
			return fileContents.toString();
		} finally {
			scanner.close();
		}
	}

	@CrossOrigin
	@RequestMapping(value = "/api/autocomplete", method = RequestMethod.GET)
	public List<String> audoComplete(@RequestParam(value = "name", defaultValue = "@NULL@") String name)
			throws FileNotFoundException {
		if (name.compareTo("@NULL@") == 0 || name.compareTo("null") == 0 || name == null || name.compareTo("") == 0) {
			return new ArrayList<String>();
		}
		List<String> rst = Application.locations.findCompletions(name);
		Collections.sort(rst, new Comparator<String>() {

			public int compare(String a, String b) {
				return a.length() - b.length();
			}

		});
		return rst.size() <= 10 ? rst : rst.subList(0, 11);
	}

	// "/api/location/predict?location=San Francisco, CA, US&rank=3&limit=100"
	// search people has sf in their top 3 location (predict)
	@CrossOrigin
	@RequestMapping(value = "/api/location/predict", method = RequestMethod.GET)
	public List<location> predictLocation(@RequestParam(value = "location", defaultValue = "null") String location,
			@RequestParam(value = "rank", defaultValue = "-1") Integer rank,
			@RequestParam(value = "prob", defaultValue = "-1.0") Double prob,
			@RequestParam(value = "count", defaultValue = "1") Integer count,
			@RequestParam(value = "limit", defaultValue = "1000") Integer limit) throws SQLException {
		GeoServiceImpl geoService = new GeoServiceImpl();
		if (location.contains("null"))
			return new ArrayList<location>();
		if (Application.locations.findCompletions(location).size() != 1)
			return new ArrayList<location>();
		if (rank == -1)
			return new ArrayList<location>();
		if (Math.abs(prob + 1.0) < 1e-8)
			prob = null;
		List<location> rst = geoService.getPredictedByLocation(location, rank, prob, count, limit * 2);
		return rst;
	}

	// "/api/location/actual?location=San Francisco, CA, US&rank=3&limit=100"
	// search people has sf in their top 3 location (actual)
	@CrossOrigin
	@RequestMapping(value = "/api/location/actual", method = RequestMethod.GET)
	public List<location> actualLocation(@RequestParam(value = "location", defaultValue = "null") String location,
			@RequestParam(value = "rank", defaultValue = "2147483647") Integer rank,
			@RequestParam(value = "prob", defaultValue = "-1.0") Double prob,
			@RequestParam(value = "count", defaultValue = "1") Integer count,
			@RequestParam(value = "limit", defaultValue = "1000") Integer limit) throws SQLException {
		GeoServiceImpl geoService = new GeoServiceImpl();
		if (location.contains("null"))
			return new ArrayList<location>();
		if (Application.locations.findCompletions(location).size() != 1)
			return new ArrayList<location>();
		if (rank == -1)
			return new ArrayList<location>();
		if (Math.abs(prob + 1.0) < 1e-8)
			prob = null;
		List<location> rst = geoService.getActualByLocation(location, rank, prob, count, limit * 2);
		return rst;
	}
	@CrossOrigin
	@RequestMapping(value = "/api/location/profile", method = RequestMethod.GET)
	public List<location> profileLocation(@RequestParam(value = "location", defaultValue = "null") String location,
			@RequestParam(value = "limit", defaultValue = "1000") Integer limit) throws SQLException {
		if (location == null)
			return new ArrayList<location>();
		List<location> rst = new ArrayList<location>();
		HttpClient httpClient = HttpClientBuilder.create().build();
		String url = "http://dsapi-stg.grid.stg.yyz.corp.pvt/api/rest/v1/actorpersona/search?dataSrc=TT&startRow=10&rows="
				+ limit + "&sortBy=screenName&sortDir=desc&fields=" + URLEncoder.encode("screenName|name|rawLocation")
				+ "&query=*:*&fq=" + URLEncoder.encode("rawLocation:\"" + location + "\"");
		HttpGet getRequest = new HttpGet(url);
		HttpResponse response = null;
		try {
			response = httpClient.execute(getRequest);

			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}

			// Get-Capture Complete application/xml body response
			BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
			StringBuilder output = new StringBuilder();
			String temp;
			while ((temp = br.readLine()) != null) {
				output.append(temp);
			}
			JSONObject obj = new JSONObject(output.toString());
			JSONArray result = obj.getJSONArray("results");
			for (int i = 0; i < result.length(); i++) {
				JSONObject cur = result.getJSONObject(i);
				location loc = new location(Long.valueOf(cur.getString("id").substring(2)), null, cur.getString("rawLocation"), null);
				loc.setName(cur.getString("name"));
				loc.setScreen_name(cur.getString("screenName"));
				rst.add(loc);
			}
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rst;

	}

}