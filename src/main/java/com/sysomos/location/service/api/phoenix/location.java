package com.sysomos.location.service.api.phoenix;

public class location {
	private final Long id;
	private final Integer rank;
	private final String location;
	private final Double support;
	private String screen_name;
	private String name;
	public location(Long id, Integer rank, String location, Integer count) {
		this.id = id;
		this.rank = rank;
		this.location = location;
		if (count != null) {
			this.support = (1 - 1 / (1.0 + count));
		}
		else support = null;
	}

	public Integer getRank() {
		return rank;
	}

	public Long getId() {
		return id;
	}

	public String getLocation() {
		return location;
	}

	public String getScreen_name() {
		return screen_name;
	}

	public void setScreen_name(String screen_name) {
		this.screen_name = screen_name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getSupport() {
		return support;
	}
}