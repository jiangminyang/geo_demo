package com.sysomos.location.service.api.phoenix;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

public class GeoServiceImpl implements GeoService {

    public static final String HASHTAG_TABLE_PREFIX = "HASHTAG";
    public static final SimpleDateFormat  EDT_FORMAT;

    static {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));        
        EDT_FORMAT = sdf;
    }
    GeoQuery geoQuery = new GeoQuery();
    
    public GeoServiceImpl() {
    }
    public void close(){
    	try {
			geoQuery.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	@Override
	public List<location> getPredictedByLocation(String location, Integer rank, Double prob, Integer count,Integer limit) throws SQLException {
		return geoQuery.queryPredictedByLocation(location, rank, prob, count, limit);
	}
	@Override
	public List<location> getActualByLocation(String location, Integer rank, Double prob, Integer count,Integer limit) throws SQLException {
		// TODO Auto-generated method stub
		return geoQuery.queryActualByLocation(location, rank, prob, count, limit);
	}
	@Override
	public List<location> getPredictedByLocation(String location, Integer rank, Integer count) throws SQLException  {
		return getPredictedByLocation(location, rank, null, count, 1000);
	}
	@Override
	public List<location> getPredictedByLocation(String location, Integer rank, Integer count, Integer limit) throws SQLException
			 {
		return getPredictedByLocation(location, rank, null, count, limit);
	}
	@Override
	public List<location> getActualByLocation(String location, Integer rank, Integer count) throws SQLException{
		// TODO Auto-generated method stub
		return getActualByLocation(location, rank, null, count, 1000);
	}
	@Override
	public List<location> getActualByLocation(String location, Integer rank, Integer count, Integer limit) throws SQLException {
		// TODO Auto-generated method stub
		return getActualByLocation(location, rank, null, count, limit);
	}

	
}