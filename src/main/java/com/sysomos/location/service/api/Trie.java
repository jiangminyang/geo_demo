package com.sysomos.location.service.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Trie {

	private TrieNode rootNode;

	public Trie() {
		super();
		rootNode = new TrieNode(' ', null);
	}

	public void load(String phrase) {
		loadRecursive(rootNode, phrase + "$", phrase);
	}

	private void loadRecursive(TrieNode node, String phrase, String text) {
		if (phrase.length() == 0) {
			return;
		}
		char firstChar = phrase.charAt(0);
		if (phrase.length() == 1) {
			node.add(firstChar, text);
		}
		else {
			node.add(firstChar, null);
		}
		TrieNode childNode = node.getChildNode(firstChar);
		if (childNode != null) {
			loadRecursive(childNode, phrase.substring(1), text);
		}
	}

	public boolean matchPrefix(String prefix) {
		TrieNode matchedNode = matchPrefixRecursive(rootNode, prefix);
		return (matchedNode != null);
	}

	private TrieNode matchPrefixRecursive(TrieNode node, String prefix) {
		if (prefix.length() == 0) {
			return node;
		}
		char firstChar = prefix.charAt(0);
		TrieNode childNode = node.getChildNode(firstChar);
		if (childNode == null) {
			// no match at this char, exit
			return null;
		} else {
			// go deeper
			return matchPrefixRecursive(childNode, prefix.substring(1));
		}
	}

	public List<String> findCompletions(String prefix) {
		TrieNode matchedNode = matchPrefixRecursive(rootNode, prefix);
		List<String> completions = new ArrayList<String>();
		findCompletionsRecursive(matchedNode, prefix, completions);
		return completions;
	}

	private void findCompletionsRecursive(TrieNode node, String prefix, List<String> completions) {
		if (completions.size() > 10) return;
		if (node == null) {
			// our prefix did not match anything, just return
			return;
		}
		if (node.getText() != null) {
			completions.add(node.getText());
		}
		if (node.contains(',')) {
			TrieNode childNode = node.getChildNode(',');
			char childChar = childNode.getNodeValue();
			findCompletionsRecursive(childNode, prefix + childChar, completions);
		}
		Collection<TrieNode> childNodes = node.getChildren();
		for (TrieNode childNode : childNodes) {
			char childChar = childNode.getNodeValue();
			if (childChar == ',') {
				continue;
			}
			findCompletionsRecursive(childNode, prefix + childChar, completions);
		}
	}

	public String toString() {
		return "Trie:" + rootNode.toString();
	}
}