package com.sysomos.location.service.api;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
@EnableAutoConfiguration(exclude=DataSourceAutoConfiguration.class)
public class Application {
	static Trie locations;
	public static void main(String[] args) throws FileNotFoundException {
//		GeoServiceImpl tst = new GeoServiceImpl();
//		System.out.println(tst.getPredictedByLocation("San Francisco, CA, US", 3, 100).toString());
//		System.out.println("");
//		System.out.println(tst.getActualByLocation("San Francisco, CA, US", 1, 100).toString());
//		
		System.out.println("start building prefix tree......");
		locations = new Trie();
		File file = new File("./resources/location.txt");
	    StringBuilder fileContents = new StringBuilder((int)file.length());
	    Scanner scanner = new Scanner(file);
	    String lineSeparator = System.getProperty("line.separator");

	    try {
	        while(scanner.hasNextLine()) {        
	            locations.load(scanner.nextLine());
	        }
	    } finally {
	        scanner.close();
	    }
	    System.out.println("finish building prefix tree");
//		try {
//			Class.forName("org.apache.phoenix.jdbc.PhoenixDriver");
//		} catch (ClassNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			// System.exit(1);
//		}
		SpringApplication.run(Application.class, args);
	}
}