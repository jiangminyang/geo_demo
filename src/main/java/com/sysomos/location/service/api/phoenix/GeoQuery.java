package com.sysomos.location.service.api.phoenix;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbcp2.BasicDataSource;

import scala.Tuple2;
import sysomos.labs.utlities.exception.EntityTypeEntityGroupMismactchException;
import sysomos.labs.utlities.exception.EntityTypeValueMismatchException;
import sysomos.labs.utlities.source.Source;
import sysomos.labs.utlities.structure.Entity.Type;
import sysomos.labs.utlities.structure.EntityGroup;
import sysomos.labs.utlities.structure.ProfileContent;
import sysomos.labs.utlities.structure.Result;
import sysomos.labs.utlities.utils.dao.ProfileUtils;

public class GeoQuery {
    private Connection connection = null;
    private Statement stmt = null;
    private ResultSet result=null;
    
//    private final String HBASE_ZOOKEEPER_LIST = GridConfiguration.getInstance().getCConfiguration().getString(ConfigConstants.GRID_ANALYTICS_HBASE_ZK_QUORUM);
    private final String HBASE_ZOOKEEPER_LIST = "alghzk01,alghzk02,alghzk03";

    private final String PHOENIX_SCHEMA = "phoenix";
    private final String PREDICTED_LOC = "SELECT * FROM PHOENIX.GEO_RESULT WHERE %s";
    private final String ACTUAL_LOC = "SELECT * FROM PHOENIX.CURRENT_LOCATION WHERE %s";
    private BasicDataSource dataSource;
    public GeoQuery() {
		dataSource = new BasicDataSource();
        dataSource.setDriverClassName("org.apache.phoenix.jdbc.PhoenixDriver");
        dataSource.setUrl("jdbc:phoenix:alghzk01,alghzk02,alghzk03");
    }
    public void close() throws Exception{
    	dataSource.close();
    }
    
    private ResultSet execuatePhoenixCommand(String command) throws SQLException{

            connection = dataSource.getConnection();
//            logger.info("Phoenix query:" + command);
//            System.out.println("command : " + command);
            stmt = connection.createStatement();

            result = stmt.executeQuery(command);
            return result;
       
    }
    
    private void closingConnect(){
        try {          
            if (stmt != null) {
                stmt.close();
                stmt = null;
            }
            if (connection != null) {
                connection.close();
                connection = null;
            }
            if(result != null){
            	result.close();
            	result = null;
            }
        } catch (Exception ex) {}
    }
    public location prepareResult(ResultSet result, boolean withCount) throws SQLException {
		Long rst_id = result.getLong("id");
		Integer rst_rank = result.getInt("rank");
		String rst_loc = result.getString("location");
		Integer rst_count = withCount ? result.getInt("count") : 0;
		
		return new location(rst_id, rst_rank, rst_loc, rst_count);
    }
    

    
    public String prepareCommand(String format, String location, Integer rank, Double probability, Integer count,  Integer limit, boolean withCount) {
    	StringBuilder query = new StringBuilder();
    	if (withCount && count != null) {
    		query.append("count >= " + count + " and ");
    	}
    	if (rank != null) {
    		query.append("rank <= " + rank + " and ");
    	}
    	if (location != null && location != "") {
    		query.append("location='" + location + "' ");
    	}
    	if (limit == null || limit > 1000) {
    		query.append("limit 1000");
    	}
    	else {
    		query.append("limit " + limit);
    	}
    	String command = String.format(format, query.toString());
    	return command;
    }
    
    public List<location> queryPredictedByLocation(String location, Integer rank, Double probability, Integer count, Integer limit) throws SQLException {
    	List<location> rst = new ArrayList<location>();
    	String command = prepareCommand(PREDICTED_LOC, location, rank, probability, count, limit, true);
    	System.out.println(command);
    	ResultSet result = execuatePhoenixCommand(command);
    	try {
        
        	while (result.next()) {
        		rst.add(prepareResult(result, true));
			}
        	List<Long> ids = new ArrayList<Long>();
        	for(location key : rst) {
        		ids.add(key.getId());
        	}
        	if (ids.size() == 0) {
        		return new ArrayList<location>();
        	}
        	EntityGroup temp = new EntityGroup(Type.USERID);
        	try {
				temp.addEntityList(ids);
			} catch (EntityTypeEntityGroupMismactchException | EntityTypeValueMismatchException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	Result<ProfileContent> results = ProfileUtils.queryForProfileByUserID(temp, Source.TWITTER);
        	Map<Long, Tuple2<String, String>> idToName = new HashMap<Long, Tuple2<String, String>>();
        	for (ProfileContent profile : results.getResults()) {
        		 idToName.put(Long.valueOf(profile.id), new Tuple2<String, String>(profile.screenName, profile.name));
        	}
        	for(int i = 0; i < rst.size(); i++) {
        		if (idToName.containsKey(rst.get(i).getId())) {
        			rst.get(i).setScreen_name(idToName.get(rst.get(i).getId())._1);
        			rst.get(i).setName(idToName.get(rst.get(i).getId())._2);
        		} 
        		else {
        			rst.get(i).setScreen_name(null);
        			rst.get(i).setName(null);
        		}
        	}
        	List<location> ans = new ArrayList<location>();
        	for(int i = 0; i < rst.size() && ans.size() < limit / 2; i++) {
        		if (rst.get(i).getScreen_name() != null && rst.get(i).getName() != null) {
        			ans.add(rst.get(i));
        		}
        	}
        	return ans;
    	} finally {
    		closingConnect();
    	}
    }
    
    public List<location> queryActualByLocation(String location, Integer rank, Double probability, Integer count, Integer limit) throws SQLException {
    	List<location> rst = new ArrayList<location>();
    	String command = prepareCommand(ACTUAL_LOC, location, rank, probability, count, limit, false);
    	System.out.println(command);
    	ResultSet result = execuatePhoenixCommand(command);
    	try {
        
        	while (result.next()) {
				rst.add(prepareResult(result, false));
			}
        	List<Long> ids = new ArrayList<Long>();
        	for(location key : rst) {
        		ids.add(key.getId());
        	}
        	if (ids.size() == 0) {
        		return new ArrayList<location>();
        	}
        	EntityGroup temp = new EntityGroup(Type.USERID);
        	try {
				temp.addEntityList(ids);
			} catch (EntityTypeEntityGroupMismactchException | EntityTypeValueMismatchException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	Result<ProfileContent> results = ProfileUtils.queryForProfileByUserID(temp, Source.TWITTER);
        	Map<Long, Tuple2<String, String>> idToName = new HashMap<Long, Tuple2<String, String>>();
        	for (ProfileContent profile : results.getResults()) {
        		 idToName.put(Long.valueOf(profile.id), new Tuple2<String, String>(profile.screenName,profile.name));
        	}
        	for(int i = 0; i < rst.size(); i++) {
        		if (idToName.containsKey(rst.get(i).getId())) {
        			rst.get(i).setScreen_name(idToName.get(rst.get(i).getId())._1);
        			rst.get(i).setName(idToName.get(rst.get(i).getId())._2);
        		} 
        		else {
        			rst.get(i).setScreen_name(null);
        			rst.get(i).setName(null);
        		}
        	}
        	List<location> ans = new ArrayList<location>();
        	for(int i = 0; i < rst.size() && ans.size() < limit / 2; i++) {
        		if (rst.get(i).getScreen_name() != null && rst.get(i).getName() != null) {
        			ans.add(rst.get(i));
        		}
        	}
        	return ans;
    	}  finally {
    		closingConnect();
    	}
    }
}