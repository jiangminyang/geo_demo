package com.sysomos.location.service.api;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

public class TrieNode {
	private String text;
	private Character character;
	private HashMap<Character, TrieNode> children;

	public TrieNode(char c, String text) {
		super();
		this.text = text;
		this.character = new Character(Character.toLowerCase(c));
		children = new HashMap<Character, TrieNode>();
	}

	public char getNodeValue() {
		return character.charValue();
	}

	public Collection<TrieNode> getChildren() {
		return children.values();
	}

	public Set<Character> getChildrenNodeValues() {
		return children.keySet();
	}

	public void add(char c, String text) {
		if (children.get(new Character(Character.toLowerCase(c))) == null) {
			// children does not contain c, add a TrieNode
			children.put(new Character(Character.toLowerCase(c)), new TrieNode(c, text));
		}
	}

	public TrieNode getChildNode(char c) {
		return children.get(new Character(Character.toLowerCase(c)));
	}

	public boolean contains(char c) {
		return (children.get(new Character(Character.toLowerCase(c))) != null);
	}

	public int hashCode() {
		return character.hashCode();
	}

	public String getText() {
		return this.text;
	}
	
	public boolean equals(Object obj) {
		if (!(obj instanceof TrieNode)) {
			return false;
		}
		TrieNode that = (TrieNode) obj;
		return (this.getNodeValue() == that.getNodeValue());
	}

}