// JavaScript Document
var app = angular.module('app',[]);
app.controller('autoCompleteCTRL', function($scope, $rootScope, $q, $http){
	"use strict";
	//Define Suggestions List
	$rootScope.suggestions = [];
	//Define Selected Suggestion Item
	$rootScope.selectedIndex = -1;
	
  $scope.previous = "";
	//Function To Call On ng-change
	$rootScope.query = function() {
		$http({
      method: 'GET',
      url: 'http://10.13.13.88:8080/api/location/predict?limit=10&rank=10&location=' + $scope.searchText,
    }).then(function(data, status, headers, config) { 
    	console.log(data.data);
      return;
    });
	}
	$rootScope.search = function(key){
		$rootScope.suggestions = [];
    if ($scope.serachText == "" || $scope.searchText == undefined) {
      return;
    }
    console.log($scope.searchText);
    $scope.previous = $scope.searchText;
		$http({
      method: 'GET',
      url: 'http://10.13.13.88:8080/api/autocomplete?name=' + $scope.searchText,
    }).then(function(data, status, headers, config) { 
    	$rootScope.suggestions = data.data
      return;
    });

	};
	
	//Keep Track Of Search Text Value During The Selection From The Suggestions List  
	$rootScope.$watch('selectedIndex',function(val){
		if(val !== -1) {
			$scope.searchText = $rootScope.suggestions[$rootScope.selectedIndex];
		}
	});
	
	
	//Text Field Events
	//Function To Call on ng-keydown
	$rootScope.checkKeyDown = function(event){

		if(event.keyCode === 40){//down key, increment selectedIndex
			event.preventDefault();
			if($rootScope.selectedIndex+1 < $rootScope.suggestions.length){
				$rootScope.selectedIndex++;
			}else{
				$rootScope.selectedIndex = 0;
			}
		}else if(event.keyCode === 38){ //up key, decrement selectedIndex
			event.preventDefault();
			if($rootScope.selectedIndex-1 >= 0){
				$rootScope.selectedIndex--;
			}else{
				$rootScope.selectedIndex = $rootScope.suggestions.length-1;
			}
		}else if(event.keyCode === 13){ //enter key, empty suggestions array
			event.preventDefault();
			$rootScope.suggestions = [];
			$rootScope.selectedIndex = -1;
		}else if(event.keyCode === 27){ //ESC key, empty suggestions array
			event.preventDefault();
			$rootScope.suggestions = [];
			$rootScope.selectedIndex = -1;
		}else{
      if ($scope.previous == $scope.searchText) return;
			$rootScope.search(event.keyCode);	
		}
	};
	
	//ClickOutSide
	// var exclude1 = document.getElementById('textFiled');
	// $rootScope.hideMenu = function($event){
	// 	$rootScope.search();
	// 	//make a condition for every object you wat to exclude
	// 	if($event.target !== exclude1) {
	// 		$rootScope.suggestions = [];
	// 		$rootScope.selectedIndex = -1;
	// 	}
	// };
	//======================================
	
	//Function To Call on ng-keyup
	$rootScope.checkKeyUp = function(event){ 
		if(event.keyCode !== 8 || event.keyCode !== 46){//delete or backspace
			if($scope.searchText === ""){
				$rootScope.suggestions = [];
				$rootScope.selectedIndex = -1;
			}
		}
	};
	//======================================
	
	//List Item Events
	//Function To Call on ng-click
	$rootScope.AssignValueAndHide = function(index){
		 $scope.searchText = $rootScope.suggestions[index];
		 $rootScope.suggestions=[];
		 $rootScope.selectedIndex = -1;
	};
	//======================================
	

});